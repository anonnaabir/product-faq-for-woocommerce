<?php

/**
 * Plugin Name:       Ultimate Product FAQ For WooCommerce-FAQable
 * Plugin URI:        https://codember.com
 * Description:       Create FAQ/Accordions using FAQable
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Codember
 * Author URI:        https://codember.com
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       faqable-lite
 * Domain Path:       /languages
 * Tested up to:      5.6.2

 * WC requires at least: 2.2
 * WC tested up to: 5.0.0

 */

    function faqable_enqueue_scripts(){
        wp_enqueue_script( 'beefup', plugins_url( 'lib/js/jquery.beefup.min.js', __FILE__ ), array('jquery'), '1.0');
        // wp_enqueue_script( 'accordion', plugins_url( 'lib/js/jquery.beefup.min.js', __FILE__ ), array('jquery'), '1.0');
        wp_enqueue_script( 'faqable', plugins_url( 'js/faqable.js', __FILE__ ), array('jquery'), '1.0');
        wp_register_style( 'beefup',    plugins_url( 'lib/css/jquery.beefup.css',    __FILE__ ), '1.0' );
        wp_enqueue_style ( 'beefup' );
    }

    add_action('wp_enqueue_scripts','faqable_enqueue_scripts');
    


    function faqable_faqs() {
        echo "
        <div>
        <article class='beefup'>
            <p class='beefup__head'>Headline</p>
            <div class='beefup__body'>My fancy collapsible content.</div>
        </article>
        <article class='beefup'>
            <p class='beefup__head'>Headline</p>
            <div class='beefup__body'>My fancy collapsible content.</div>
        </article>
        </div>
        ";
    }

 add_action('woocommerce_before_add_to_cart_form','faqable_faqs');